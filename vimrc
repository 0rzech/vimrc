" Author: Piotr Orzechowski [orzechowski.tech]

set nocp                        "Disable Vi compatibility
set autoindent                  "Automatically indent new lines
set number                      "Show line numbers
set ruler                       "Show ruler
set showcmd                     "Show incomplete commands
set showmode                    "Show edit mode
set tabstop=4                   "A tab is 4 spaces
set expandtab                   "Always uses spaces instead of tabs
set softtabstop=4               "Insert 4 spaces when tab is pressed
set shiftwidth=4                "An indent is 4 spaces
set smarttab                    "Indent instead of tab at start of line
set shiftround                  "Round spaces to nearest shiftwidth multiple
set nojoinspaces                "Don't convert spaces to tabs
set colorcolumn=100             "Show right margin
set clipboard=unnamedplus       "Always use system clipboard
set hlsearch                    "Highlight matching words
set incsearch                   "Forward search during typing
set ic                          "Ignore case by default
set backspace=indent,eol,start  "Allow BS to remove these in front of the cursor
set t_Co=256                    "Enable 256 color support
set backupdir=~/.vim/backups    "Keep backups here
set nomodeline                  "Disable modelines due to security reasons

syntax on                       "Enable syntax highlighting
filetype plugin indent on

let mapleader=','               "Set <Leader> to ,
map Q gq                        "Map Q to do gq formatting

"Select block just after putting it
nnoremap <expr> gV "`[".getregtype(v:register)[0]."`]"

colorscheme eclipsemintdark

let g:netrw_liststyle=1         "Show file details by default
"Hide .* files by default
let g:netrw_list_hide='\(^\|\s\s\)\zs\.\S\+,\(^\|\s\s\)ntuser\.\S\+'
let g:netrw_banner=0            "Hide banner
let g:netrw_sort_options='i'    "Ignore case when sorting files

au BufRead,BufNewFile *.ex,*.exs set filetype=ruby
au BufRead,BufNewFile *.app set filetype=erlang
